package see.jarreaper;

import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        initArgs(args);
    }

    private static Properties initArgs(String[] args) {
        Properties properties = new Properties();
        for (String arg : args) {
            String[] keyValue = arg.split("=");
            String key = keyValue[0];
            String value = keyValue.length > 1
                    ? keyValue[0]
                    : null;
            properties.put(key, value);
        }
        return properties;
    }
}