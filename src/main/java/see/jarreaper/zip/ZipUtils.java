package see.jarreaper.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtils {
    public static File getDirFromJar(File parent, String root, URL pathToJar) {
        if (pathToJar.getProtocol().equals("jar")) {
            String[] resourcesPath = pathToJar.getFile().split("file:/|!");
            String jarPath = resourcesPath[1];
            String dirPathIntoJar = resourcesPath[2];
            try {
                ZipInputStream zis = new ZipInputStream(new FileInputStream(jarPath));
                ZipEntry zipEntry;
                while ((zipEntry = zis.getNextEntry()) != null) {
                    if (!zipEntry.getName().startsWith(root)) {
                        continue;
                    }
                    createFile(parent, zipEntry, zis);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return new File(parent, root);
        }
        return new File(pathToJar.getFile());
    }

    public static byte[] getEntry(File archive, String entry) {
        byte[] result = null;
        try {
            ZipInputStream zis = new ZipInputStream(new FileInputStream(archive));
            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                if (!zipEntry.getName().startsWith(entry)) {
                    continue;
                }
                result = new byte[(int) zipEntry.getSize()];
                zis.read(result);

            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public static List<String> getFileNames(File archive) {
        List<String> result = new ArrayList<>();
        try {
            ZipInputStream zis = new ZipInputStream(new FileInputStream(archive));
            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                String name = zipEntry.getName();
                if (!name.endsWith("/")) {
                    result.add(zipEntry.getName());
                }

            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public static void createFile(File parent, ZipEntry zipEntry, ZipInputStream zis) {
        try {
            if (zipEntry.isDirectory()) {
                createDir(parent, zipEntry.getName());
            } else {
                createFile(parent, zipEntry.getName(), zis);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void createDir(File parent, String dirName) throws IOException {
        File currentDir = new File(parent, dirName);
        if (!currentDir.exists()) {
            boolean hasBeenCreated = currentDir.mkdirs();
            if (!hasBeenCreated) {
                throw new IOException("Can not create dir %s".formatted(currentDir.getAbsolutePath()));
            }
        }
    }

    public static void createFile(File parent, String fileName, ZipInputStream zis) throws IOException {
        File currentFile = new File(parent, fileName);
        if (!currentFile.exists()) {
            boolean hasBeenCreated = currentFile.createNewFile();
            if (!hasBeenCreated) {
                throw new IOException("Can not create file %s".formatted(currentFile.getAbsolutePath()));
            }
        }
        try (FileOutputStream fos = new FileOutputStream(currentFile)) {
            byte[] buffer = new byte[1024];
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
        }
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());
        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();
        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }
        return destFile;
    }
}
