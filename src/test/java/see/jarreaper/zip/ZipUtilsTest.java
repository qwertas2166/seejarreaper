package see.jarreaper.zip;

import org.junit.jupiter.api.Test;
import see.testicle.api.SeeTestSupportApi;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZipUtilsTest implements SeeTestSupportApi {
    @Test
    public void getEntry() {
        File archive = this.getFile(this.getTestRecoursesDir(this.getClass()) + "test.jar");
        byte[] result = ZipUtils.getEntry(archive, "META-INF/qwe.txt");
        assertEquals(new String(result, StandardCharsets.UTF_8), "qweasd");
    }

    @Test
    public void getEntryList() {
        File archive = this.getFile(this.getTestRecoursesDir(this.getClass()) + "test.jar");
        List<String> result = ZipUtils.getFileNames(archive);
        assertEquals(result, List.of("LICENSE.txt", "META-INF/qwe.txt"));
    }
}
